let renderItem = (list) => {
    let contentToDo = "";
    let contentCompleted = "";
    list.forEach((item, index) => {
        if (item.isCompleted == false) {
            return (contentToDo += `
            <li>
            ${item.content}
            <div class="buttons">
            <button onclick="removeTask(${index})" class="remove"><i class="fa fa-trash-alt"></i></button>
            <button onclick="completeTask(${index})" class="complete"><i class="fa fa-check-circle"></i></button>
            </div>
            </li>`);
        }
    });
    list.forEach((item, index) => {
        if (item.isCompleted == true) {
            return (contentCompleted += `
            <li>
            ${item.content}
            <div class="buttons">
            <button onclick="removeTask(${index})" class="remove"><i class="fa fa-trash-alt"></i></button>
            </div>
            </li>`);
        }
    });
    document.getElementById("todo").innerHTML = contentToDo;
    document.getElementById("completed").innerHTML = contentCompleted;
};

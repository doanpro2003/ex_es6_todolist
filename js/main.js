let newTasks = [];
let JSONnewTasks = localStorage.getItem("dsNewTasks");
if (JSONnewTasks) {
    newTasks = JSON.parse(JSONnewTasks);
    renderItem(newTasks);
}

let saveTasks = () => {
    var JSONnewTasks = JSON.stringify(newTasks);
    localStorage.setItem("dsNewTasks", JSONnewTasks);
};

function addItem() {
    let taskContent = document.getElementById("newTask").value;
    if (taskContent == "") return;
    let newTask = new ToDo(taskContent, false);

    newTasks.push(newTask);

    renderItem(newTasks);
    saveTasks();
    document.getElementById("newTask").value = "";
}
function removeTask(index) {
    newTasks.splice(index, 1);
    saveTasks();
    renderItem(newTasks);
}

let completeTask = (index) => {
    newTasks[index].isCompleted = true;

    saveTasks();

    renderItem(newTasks);
};

document.getElementById("az").addEventListener("click", () => {
    newTasks = newTasks.sort((a, b) => {
        var textA = a.content.toUpperCase();
        var textB = b.content.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
    });
    renderItem(newTasks);
    saveTasks();
});
document.getElementById("za").addEventListener("click", () => {
    newTasks = newTasks.sort((a, b) => {
        var textA = a.content.toUpperCase();
        var textB = b.content.toUpperCase();
        return textA > textB ? -1 : textA < textB ? 1 : 0;
    });
    renderItem(newTasks);
    saveTasks();
});

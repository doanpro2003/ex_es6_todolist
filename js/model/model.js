class ToDo {
    constructor(_content, _isCompleted) {
        this.content = _content;
        this.isCompleted = _isCompleted;
    }
}
